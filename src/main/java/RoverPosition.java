import java.util.HashMap;

/*
* Created By Priyank Kabra (priyank.kabra@cleartrip.com)
*/

public class RoverPosition {
    /* stores coordinates and initial facing direction of rover*/
    public static int x, y, direction;

    /* stores upper coordinates of plateau */
    public static int maxX, maxY;

    /* array indices act as numberical value for diretion-facing of rover */
    final static char intToCharDirection[] = {'N', 'E' , 'S', 'W'};

    /* stores numerical value for each character representing direction-facing */
    final static HashMap<Character, Integer> charToIntDirection = new HashMap<Character, Integer>();

    static
    {
        charToIntDirection.put('N', 0);
        charToIntDirection.put('E', 1);
        charToIntDirection.put('S', 2);
        charToIntDirection.put('W', 3);
    }

    public static void main(String gg[]) throws Exception
    {
        java.io.BufferedReader bufferedReader = new java.io.BufferedReader(new java.io.InputStreamReader(System.in));

        /* first line of input gives space seperated upper coordinates of plateau (example -> 5 5)*/
        String input[] = (bufferedReader.readLine()).split(" ");

        try
        {
            maxX = Integer.parseInt(input[0]);
            maxY = Integer.parseInt(input[1]);
        }

        catch(Exception e)
        {
            e.printStackTrace();
        }

        /* second line of input gives space seperated initial coordinates and direction-facing for rover
               (example -> 1 2 N) */
        input = (bufferedReader.readLine()).split(" ");

        try
        {
            x = Integer.parseInt(input[0]);
            y = Integer.parseInt(input[1]);

            /* direction-facing stored as integer */
            direction = charToIntDirection.get((input[2]).charAt(0));
        }

        catch(Exception e)
        {
            e.printStackTrace();
        }

        /* third line of input gives instruction string */
        String instructions = bufferedReader.readLine();

        /* variable to store the final position of rover */
        String result = null;

        try {
            /* method call to process the instructions */
            result = processInstructions(instructions, maxX, maxY);
        }

        catch(Exception e)
        {
            e.printStackTrace();
        }

        System.out.println(result);
    }

    /* method that process the instructions */
    public static String processInstructions(String instructions, int maxX, int maxY) throws Exception
    {
        /* Logic -> N = 0, E = 1, S = 2, W = 3
           L = face 90 degree left
           R = face 90 degree right
           M = move 1 step forward
        */

        /* Traverse through the instruction string character by character */
        for(int i=0; i<instructions.length(); i++)
        {
            switch(instructions.charAt(i))
            {
                case 'L':
                    direction = (direction + 3) % 4;
                    break;

                case 'R':
                    direction = (direction + 1) % 4;
                    break;

                case 'M':
                    switch(direction) {
                        case 0:
                            y++;

                            if(y > maxY)
                                throw new InvalidInstructionException("The given instructions sends the rover out of upper coordinate range");

                            break;

                        case 1:
                            x++;

                            if(x > maxX)
                                throw new InvalidInstructionException("The given instructions sends the rover out of upper coordinate range");

                            break;

                        case 2:
                            y--;

                            if(y < 0)
                                throw new InvalidInstructionException("The given instructions sends the rover out of lower coordinate range");

                            break;

                        case 3:
                            x--;

                            if(x < 0)
                                throw new InvalidInstructionException("The given instructions sends the rover out of lower coordinate range");

                            break;
                    }
                    break;

                default:
                    throw new InvalidInstructionException(instructions.charAt(i) + " is not allowed");
            }
        }

        /* return final position in space seperated way (example -> 1 3 N) */
        return (x + " " + y + " " + intToCharDirection[direction]);
    }
}