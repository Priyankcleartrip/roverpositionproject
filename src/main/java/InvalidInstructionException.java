/* Custom Exception */
public class InvalidInstructionException extends Exception {
    InvalidInstructionException(String s)
    {
        super(s);
    }
}